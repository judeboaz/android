package com.example.jude.lab5;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView greenList, yellowList;
    private String[] greenTitles, yellowTitles;
    private int[] greenIcons, yellowIcons;
    private String[] yellowLetters = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        greenList = findViewById(R.id.greenList);
        yellowList = findViewById(R.id.yellowList);

        greenIcons = getGreenIcons();
        yellowIcons = getYellowIcons();

        Resources res = getResources();

        greenTitles = res.getStringArray(R.array.green_titles);
        yellowTitles = res.getStringArray(R.array.yellow_titles);

        greenList = findViewById(R.id.greenList);

        GreenListAdapter greenAdapter = new GreenListAdapter(this, greenTitles, greenIcons);
        greenList.setAdapter(greenAdapter);
        greenList.setOnItemClickListener(new GreenListener());

        YellowListAdapter yellowAdapter = new YellowListAdapter(this, yellowTitles, yellowIcons, yellowLetters);
        yellowList.setAdapter(yellowAdapter);
        yellowList.setOnItemClickListener(new YellowListener());
    }

    class GreenListener implements OnItemClickListener{
        GreenListener(){}

        // long is included to make implementation happy
        public void onItemClick(AdapterView<?> adaptView, View view, int i, long l){
            Intent intent = new Intent(MainActivity.this, GreenListDetail.class);
            intent.putExtra("image", greenIcons[i]);
            intent.putExtra("item", greenTitles[i]);
            startActivity(intent);
        }
    }

    class YellowListener implements OnItemClickListener{
        YellowListener(){}

        // long is included to make implementation happy
        public void onItemClick(AdapterView<?> adaptView, View view, int i, long l){
            Intent intent = new Intent(MainActivity.this, YellowListDetail.class);
            intent.putExtra("letter", yellowLetters[i]);
            intent.putExtra("item", yellowTitles[i]);
            intent.putExtra("image", yellowIcons[i]);
            startActivity(intent);
        }
    }

    private int[] getGreenIcons(){
        int[] icons = { R.drawable.santaclaus, R.drawable.rainumbrella, R.drawable.picketfence, R.drawable.spaceship,
                R.drawable.foreignpassport, R.drawable.flowericon, R.drawable.snowman, R.drawable.skullbones,
                R.drawable.flowerpot, R.drawable.librascale, R.drawable.teddybear, R.drawable.diamondring};
        return icons;
    }

    private int[] getYellowIcons(){
        int[] icons = { R.drawable.cigarettelighter, R.drawable.househome, R.drawable.desklamp, R.drawable.magicman,
                R.drawable.lightbulb, R.drawable.handcuffs, R.drawable.acousticguitar, R.drawable.smokinggun,
                R.drawable.hairdryer, R.drawable.snoopydog, R.drawable.stopwatch, R.drawable.floppydisk};
        return icons;
    }
}


class GreenListAdapter extends ArrayAdapter {
    Context context;
    int[] images;
    String[] iconTitles;

    GreenListAdapter(Context c, String[] titles, int imgs[]){
        super(c, R.layout.single_row, R.id.textView, titles);

        this.context = c;
        this.images = imgs;
        this.iconTitles = titles;
    }

   /* @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.single_row, parent, false);
        ImageView image = row.findViewById(R.id.imageView2);
        TextView text = row.findViewById(R.id.textView);

        image.setImageResource(images[position]);
        text.setText(iconTitles[position]);

        return row;
    }*/
}

class YellowListAdapter extends ArrayAdapter {
    Context context;
    int[] images;
    String[] iconTitles;
    String[] letters;

    YellowListAdapter(Context c, String[] titles, int imgs[], String[] givenLetters){
        super(c, R.layout.single_row, R.id.textView, titles);

        this.context = c;
        this.images = imgs;
        this.iconTitles = titles;
        this.letters = givenLetters;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.single_row_yellow, parent, false);
        ImageView image = row.findViewById(R.id.yellowIcon);
        TextView text = row.findViewById(R.id.yellowItemNameTextView);
        TextView letter = row.findViewById(R.id.yellowLetterTextView);

        image.setImageResource(images[position]);
        text.setText(iconTitles[position]);
        letter.setText(letters[position]);

        return row;
    }
}
