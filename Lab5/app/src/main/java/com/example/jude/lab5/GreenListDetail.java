package com.example.jude.lab5;

/**
 * Created by Jude on 6/14/2018.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class GreenListDetail extends AppCompatActivity {

    ImageView image;
    TextView text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_green);

        Intent intent = getIntent();

        String item = intent.getStringExtra("item");
        text = findViewById(R.id.textView2);
        text.setText(item);

        int imgId = intent.getIntExtra("image", 0);
        image = findViewById(R.id.imageView);
        image.setImageResource(imgId);
    }
}
