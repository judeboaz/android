package com.example.jude.lab5;

/**
 * Created by Jude on 6/14/2018.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class YellowListDetail extends AppCompatActivity {

    ImageView image;
    TextView text;
    TextView letter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yellow);

        Intent intent = getIntent();

        String item = intent.getStringExtra("item");
        text = findViewById(R.id.textView2);
        text.setText(item);

        String letterString = intent.getStringExtra("letter");
        letter = findViewById(R.id.textView3);
        letter.setText(letterString);

        int imgId = intent.getIntExtra("image", 0);
        image = findViewById(R.id.imageView);
        image.setImageResource(imgId);
    }
}
